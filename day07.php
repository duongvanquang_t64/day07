<?php
$phankhoa = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .items {
            display: flex;
            align-items: center;
            margin-left: 60px;
        }

        .phankhoa {
            height: 35px;
            border: 1px solid #007fff;
            margin-left: 20px;
            width: 200px;
        }

        .search {
            width: 300px;
            height: 35px;
            border: 1px solid #007fff;
            margin-left: 20px;
        }

        .btn {
            padding: 15px;
            background-color: #00bf00;
            border: 1px solid #007fff;
            border-radius: 10px;
            margin-left: 250px;

        }

        table {
            width: 100%;
            border-collapse: separate;
            border-spacing: 0 1em;
        }

        td,
        th {
            /* border: 1px solid #dddddd; */
            text-align: left;
            padding: 6px;
        }

        form td {
            text-align: center;
        }

        .wrap-action {
            width: 90%;
            display: flex;
            justify-content: end;
        }

        .action {

            display: inline-block;
            padding: 10px;
            background-color: #00bf00;
            border: 1px solid #007bc7;
            text-decoration: none;
            border-radius: 10px;
            margin-right: 8px;
        }


        .content {}
    </style>
</head>

<body>
    <div>
        <form>
            <div class="items">
                <p class="content">
                    <label for="phankhoa">
                        Khoa
                    </label>
                </p>
                <select name="phankhoa" id="phankhoa" class="phankhoa">
                    <option disabled selected value></option>
                    <?php foreach ($phankhoa as $key => $value) : ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="items">
                <p>Từ Khoá</p>
                <input type="text" class="search">
            </div>
            <div>
                <input type="submit" class="btn" value="Tìm Kiếm">
            </div>
        </form>
    </div>
    <div class="kq_search" style="margin-top: 30px; margin-left: 20px;">
        <span>Số sinh viên tìm thấy: </span>
        <span> XXX</span>
    </div>
    <div>
        <div class="list-student">
            <div class="wrap-action">
                <a class="action" href="./day05.php">Thêm</a>
            </div>
            <div class="table-student">
                <table>
                    <colgroup>
                        <col span="1" style="width: 10%;">
                        <col span="1" style="width: 20%;">
                        <col span="1" style="width: 40%;">
                        <col span="1" style="width: 15%;">
                    </colgroup>
                    <tr>
                        <th>No</th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <a class="action" href="">Xóa</a>
                            <a class="action" href="">Sửa</a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Trần Thị B</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <a class="action" href="">Xóa</a>
                            <a class="action" href="">Sửa</a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Nguyễn Hoàng C</td>
                        <td>Khoa học vật liệu</td>
                        <td>
                            <a class="action" href="">Xóa</a>
                            <a class="action" href="">Sửa</a>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Đinh Quang D</td>
                        <td>Khoa học vật liệu</td>
                        <td>
                            <a class="action" href="">Xóa</a>
                            <a class="action" href="">Sửa</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
</body>

</html>