<?php
$gioitinh = array(0 => "Nam", 1 => "Nữ");

$phankhoa = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");





?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        .room {
            width: 750px;
            height: 600px;
            display: block;
            border: 1px solid #007fff;
            margin: auto;
            margin-top: 50px;


        }

        .items {
            display: flex;
            align-items: center;
            margin-left: 60px;
        }

        i {
            color: red;
        }

        .content {
            width: 90px;
            height: 20px;
            padding: 10px 0px;
            text-align: center;
            display: block;
            background-color: #00bf00;
            border: 1px solid #007fff;
        }

        .iptext {
            width: 300px;
            height: 35px;
            border: 1px solid #007fff;
            margin-left: 20px;
        }

        .ipdate {
            height: 35px;
            border: 1px solid #007fff;
            width: 200px;
            margin-left: 20px;
        }

        .phankhoa {
            height: 35px;
            border: 1px solid #007fff;
            margin-left: 20px;
            width: 200px;
        }

        .ipgioitinh {
            margin-left: 20px;
        }

        .btn {
            padding: 15px;
            background-color: #00bf00;
            border: 1px solid #007fff;
            border-radius: 10px;
            margin-left: 250px;

        }

        .ipimage {
            margin-left: 20px;
        }

        .post {
            color: red;
            margin-left: 10px;
            margin-top: 20px;
        }

        #error-box {
            margin-left: 60px;
            color: red;
            display: flex;

        }

        .error {
            margin-left: 10px;
        }
    </style>
    <script>
        function validateForm() {
            let registerForm = document.forms['form']
            var fullName = form["name"].value
            var gioitinh = form["gioitinh"].value
            var phankhoa = form["phankhoa"].value
            var ngaysinh = form["ngaysinh"].value
            var diachi = form["diachi"].value
            var errorVal = []
            var image = form["file"].value
            if (!fullName.length) {
                errorVal.push("Hãy nhập tên.")
            }
            if (gioitinh === '') {
                errorVal.push("Hãy nhập giới tính.")
            }
            if (!phankhoa) {
                errorVal.push("Hãy nhập phân khoa")
            }
            if (!ngaysinh) {
                errorVal.push("Hãy nhập ngày sinh.")
            }
            if (!diachi) {
                errorVal.push("Hãy nhập địa chỉ.")
            }
            if (image) {
                let fileExt = image.split('.').pop().toLowerCase()
                if (!allowedExtension.includes(fileExt)) {
                    errorVal.push("Hình ảnh phải là định dạng " + allowedExtension.join('/'))
                }
            }
            if (errorVal.length > 0) {
                let errorBox = document.getElementById('error-box')
                let errorsHtml = []
                errorBox.innerHTML = ''
                errorVal.forEach(error => {
                    errorsHtml.push(`<p class="error">${error}</p>`)
                })
                errorBox.innerHTML = errorsHtml.join('')
                return false
            }
        }
    </script>
</head>

<body>
    <div>
        <form onsubmit="return validateForm()" action="confim.php" method="post" enctype="multipart/form-data" name="form">
            <div class="room">
                <div id="error-box">
                </div>
                <div class="items" style="margin-top: 40px;">
                    <p class="content">Họ và Tên<i>*</i></p>
                    <input type="text" class="iptext" class="name" name="name" id="name">

                </div>
                <div class="items">
                    <p class="content" style="margin-right: 20px;">Giới tính<i>*</i></p>

                    <?php foreach ($gioitinh as $key => $value) : ?>
                        <label for=<?= $key ?>>
                            <?= $value; ?>
                        </label>
                        <input id=<?= $key ?> type="radio" name="gioitinh" class="gioitinh" value=<?= $key ?>>
                    <?php endforeach; ?>


                </div>
                <div class="items">
                    <p class="content">
                        <label for="phankhoa">
                            Phân khoa
                        </label>
                        <i>*</i>
                    </p>
                    <select name="phankhoa" id="phankhoa" class="phankhoa">
                        <option disabled selected value></option>
                        <?php foreach ($phankhoa as $key => $value) : ?>
                            <option value="<?= $key ?>"><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>

                </div>
                <div class="items">
                    <p class="content">Ngày sinh<i>*</i></p>
                    <input type="date" class="ipdate" name="ngaysinh">

                </div>
                <div class="items">
                    <p class="content">Địa chỉ</p>
                    <input type="text" class="iptext" name="diachi">
                </div>
                <div class="items">
                    <p class="content">
                        <label for="file">
                            Hình ảnh
                        </label>
                    </p>
                    <input id="file" name="file" type="file" class="ipimage">
                </div>
                <div>
                    <input type="submit" value="Đăng Kí" class="btn" name="submit">
                </div>

            </div>
        </form>
    </div>

</body>

</html>